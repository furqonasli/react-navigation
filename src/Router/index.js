import React from 'react'
import { createStackNavigator} from '@react-navigation/stack'

import { Login, Register, Splash, WelcomeAuth, Home} from '../Pages';


const Stack = createStackNavigator();

const Router = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen options={{headerShown: false}} name="Splash" component={Splash} />
            <Stack.Screen options={{headerShown: false}} name="Login" component={Login} />
            <Stack.Screen options={{headerShown: false}} name="Register" component={Register} />
            <Stack.Screen options={{headerShown: false}} name="WelcomeAuth" component={WelcomeAuth} />
            <Stack.Screen name="Home" component={Home} />
        </Stack.Navigator>
    )
}

export default Router
