import React from 'react'
import { View, Text } from 'react-native'

const Home = () => {
    return (
        <View style={{justifyContent:'center',alignItems:'center',flex:1}}>
            <Text style={{color:'#02006C',fontWeight:'bold', fontSize:40}}>Home Page</Text>
        </View>
    )
}

export default Home
