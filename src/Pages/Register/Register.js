import React from 'react'
import { View, Text, TouchableOpacity} from 'react-native'

const Register = ({navigation}) => {
    return (
        <View style={{justifyContent:'center',alignItems:'center',flex:1}}>
            <Text style={{color:'#02006C',fontWeight:'bold', fontSize:40, marginBottom:20}}>Register</Text>
            <TouchableOpacity 
                onPress={() => {
                    navigation.replace('Home')
                }}
            >
                <Text style={{
                    color:'#fff',
                    backgroundColor:'#02006C',
                    paddingHorizontal:50,
                    paddingVertical:10,
                    borderRadius:5,
                    fontSize:20,
                    fontWeight:'700',
                }}>
                    Daftar
                </Text>
            </TouchableOpacity>
        </View>
    )
}

export default Register
